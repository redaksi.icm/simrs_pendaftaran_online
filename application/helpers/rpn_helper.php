<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

  if(!function_exists('isUrlExist')) {
    function isUrlExist($url){
      $ch = curl_init($url);    
      curl_setopt($ch, CURLOPT_NOBODY, true);
      curl_exec($ch);
      $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      if($code == 200){
        $status = true;
      }else{
        $status = false;
      }
      curl_close($ch);
      
      return $status;
    }
  }

  if(!function_exists('toDate')) {
    function toDate($date)
    {
      if ($date == '' || $date == null) {
        return '';
      }else{
        $raw = explode("-", $date);
        return $raw[2].'-'.$raw[1].'-'.$raw[0];
      }
    }
  }

  if(!function_exists('dateSys2dateId')) {
    function dateSys2dateId($date)
    {
      if($date == '' || $date == null){
        return '';
      }else{
        $raw = explode("-", $date);
        return $raw[2].'-'.$raw[1].'-'.$raw[0];
      }
    }
  }

  if(!function_exists('dateDiff')) {
    function dateDiff($date_1 , $date_2 , $differenceFormat = '%a' ){
      $datetime1 = date_create($date_1);
      $datetime2 = date_create($date_2);
      
      $interval = date_diff($datetime1, $datetime2);
      
      return $interval->format($differenceFormat);
    }
  }

  if(!function_exists('monthName')) {
    function monthName($m,$lg = 'ID'){
      $m = intval($m)-1;
      $data['id'] = array(
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
      );
      if ($m<0 || $m>12) {
        return '';
      }else {
        return $data[$lg][$m];
      }
    }
  }

  if(!function_exists('listMonthId')) {
    function listMonthId(){
      $data = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember',
      );
      return $data;
    }
  }

  if(!function_exists('dateId')) {
    function convertDateId($tgl='') {
      if($tgl != '') {
          $tanggal = substr($tgl, 8, 2);
          $jam = substr($tgl, 11, 8);
          $bulan = monthName(substr($tgl, 5, 2));
          $tahun = substr($tgl, 0, 4);
          if($jam != '') {
              return $tanggal . ' ' . $bulan . ' ' . $tahun . ' ' . $jam;
          } else {
              return $tanggal . ' ' . $bulan . ' ' . $tahun;
          }    
      }    
    }
  }

  if (!function_exists('timeElapsed')) {
    function timeElapsed($datetime, $full = false) {
      $now = new DateTime;
      $ago = new DateTime($datetime);
      $diff = $now->diff($ago);

      $diff->w = floor($diff->d / 7);
      $diff->d -= $diff->w * 7;

      $string = array(
        'y' => 'tahun',
        'm' => 'bulan',
        'w' => 'minggu',
        'd' => 'hari',
        'h' => 'jam',
        'i' => 'menit',
        's' => 'detik',
      );
      foreach ($string as $k => &$v) {
        if ($diff->$k) {
          $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        } else {
          unset($string[$k]);
        }
      }

      if (!$full) $string = array_slice($string, 0, 1);
      return $string ? implode(', ', $string) . ' yg lalu' : 'baru saja';
    }
  }

  if (!function_exists('wordLimiter')){
    function wordLimiter($str, $limit = 100, $end_char = '&#8230;'){
      if (trim($str) === ''){
        return $str;
      }

      preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);

      if (strlen($str) === strlen($matches[0])){
        $end_char = '';
      }

      return rtrim($matches[0]).$end_char;
    }
  }

  if (!function_exists('numId')){
    function numId($v){
      $res = str_replace('.', '', $v);
      $res = str_replace(',', '.', $res);
      return $res;
    }
  }

  if (!function_exists('numSys')){
    function numSys($v)
    {
      if(is_numeric($v)){
        $res = number_format($v, 0, ",", ".");
        return $res;
      }else{
        return $v;
      }
    }
  }

  if(!function_exists('createLog')) {
    function createLog($access = 1, $module = "")
    {
      $CI = get_instance();
      
      $acc = $CI->db->where('id', $access)->get('_access')->row_array();

      if ($CI->agent->is_browser()){
        $agent = $CI->agent->browser().' '.$CI->agent->version();
      }elseif ($CI->agent->is_robot()){
        $agent = $CI->agent->robot();
      }elseif ($CI->agent->is_mobile()){
        $agent = $CI->agent->mobile();
      }else{
        $agent = 'Unidentified';
      }

      $data = array(
        'user_id' => @$CI->session->userdata('user_id'),
        'session_id' => @$CI->session->session_id,
        'fullname' => @$CI->session->userdata('fullname'),
        'access' => @$acc['access'],
        'ip_address' => @$CI->input->ip_address(),
        'user_agent' => @$agent,
        'platform' => @$CI->agent->platform(),
        'module' => @$module,
        'url' => @current_url(),
        'description' => @$acc['description'],
        'created' => @date('Y-m-d H:i:s')
      );

      $file = APPPATH.'logs/access-'.date('Y-m-d').'.json';
      $log = json_decode(read_file($file));
      if ($log === null) {$log = array();};
      array_push($log,$data);
      write_file($file, json_encode($log), 'w+');
    }
  }

  if(!function_exists('getCookieMenu')) {
    function getCookieMenu($controller)
    {
      $CI = get_instance();
      if (is_null(get_cookie($controller))) {
        $val = array(
          'search' => null,
          'per_page' => null,
          'cur_page' => null,
          'total_rows' => null,
          'order' => null
        );
        $cookie = array(
          'name'   => $controller,
          'value'  => json_encode($val),
          'expire' => '300'
        );
        $CI->input->set_cookie($cookie);
        return $val;
      }else{
        return json_decode(get_cookie($controller),TRUE);
      }
    }
  }

  if(!function_exists('setCookieMenu')) {
    function setCookieMenu($controller, $cookie_val)
    {
      $CI = get_instance();
      $cookie = array(
        'name'   => $controller,
        'value'  => json_encode($cookie_val),
        'expire' => '300'
      );
      $CI->input->set_cookie($cookie);
    }
  }

  if(!function_exists('setPagination')) {
    function setPagination($controller, $data)
    {
      $CI = get_instance();
      $config['per_page'] = $data['per_page'];
      $config['base_url'] = site_url().'/'.$controller.'/index';
      $config['total_rows'] = $data['total_rows'];
      $CI->pagination->initialize($config);
    }
  }

  if(!function_exists('paginationInfo')) {
    function paginationInfo($list_rows, $data)
    {
      $str = "Menampilkan ";
			if ($list_rows == 0) {
				$str .= '0 sampai 0 dari 0 data.';
			}else{
				if ($list_rows > 0) {
					$str .= ($data['cur_page']+1);
				}else{
					$str .= ($data['cur_page']);
				}
				$str .= " sampai ".($data['cur_page']+$list_rows)." dari ".$data['total_rows']." data.";
      }
      return $str;
    }
  }

  if (!function_exists('getParameter')){
    function getParameter($parameter)
    {
      $CI = get_instance();
      $CI->load->model('_parameter/m_parameter');
      $parameter = $CI->m_parameter->by_field('parameter', $parameter);
      if ($parameter == null) {
        return '<<parameter not found!>>';
      }
      return $parameter['value'];
    }
  }
  
  if (!function_exists('excelStyle')) {
    function excelStyle()
    {
      $data = array();
      //align center
      $data['align_center'] = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
      );
      //align left
      $data['align_left'] = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        )
      );
      //align right
      $data['align_right'] = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        )
      );
      //font bold
      $data['font_bold'] = array(
        'font' => array(
          'bold'  => 'bold'
        )
      );
      //font underline
      $data['font_underline'] = array(
        'font' => array(
          'underline'  => 'underline'
        )
      );
      //font size 12
      $data['font_size_12'] = array(
        'font' => array(
          'size'  => 12
        )
      );
      //wrap text
      $data['wrap_text'] = array(
        'alignment' => array(
          'wrap' => 'wrap',
        )
      );
      $data['bg_yellow'] = array(
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'color' => array('rgb' => 'FFFF00')
        )
      );
      $data['font_red'] = array(
        'font'  => array(
          'color' => array('rgb' => 'FF0000'),
        )
      );
      //create border
      $data['styleArray'] = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '111111'),
          ),
        ),
        'font' => array(
          'size'  => 11,
          'name'  => 'Calibri'
        ),
      );

      return $data;
    }
    
  }

  if (!function_exists('getCountDay')){
    function getCountDay($month='', $year='')
    {
      if ($month !='' && $year !='') {
        $count_date = cal_days_in_month(CAL_GREGORIAN,$month,$year);
      }else{
        $count_date = cal_days_in_month(CAL_GREGORIAN,01,2020);
      }

      return $count_date;
    }
  }

  if(!function_exists('countAge')) {
    function countAge($tanggal_lahir) {
      list($year,$month,$day) = explode("-",$tanggal_lahir);
      $year_diff  = date("Y") - $year;
      $month_diff = date("m") - $month;
      $day_diff   = date("d") - $day;
      if ($month_diff < 0) $year_diff--;
          elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
      return $year_diff;
    }
  }

  if (!function_exists('webservice')){
    function webservice($port,$url,$parameter){
      $curl = curl_init();
      set_time_limit(0);
      curl_setopt_array($curl, array(
        CURLOPT_PORT => $port,
        CURLOPT_URL => "http://".$url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $parameter,
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
          "content-type: application/x-www-form-urlencoded"
          ),
        )
      );
      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);
      
      if ($err) {
        $response = ("Error #:" . $err);
      }else{
        $response;
      }
      
      return $response;
    }
  }

  if (!function_exists('js_chosen')){
    function js_chosen() {    
      $html = '<script>
               $(function() {
                  $(".select2").select2();
               });
               </script>';
      return $html;
    }
  }

  if (!function_exists('captcha')){
    function captcha($length = 6) {
      $characters = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
      }
      return $randomString;
    }
  }

  if (!function_exists('get_id')){
    function get_id($modul='')
    {
      $CI = get_instance();

      $date_now = date('Y-m-d');
      $id = $CI->db->query("SELECT * FROM tmp_id WHERE modul = '$modul' AND tgl_id = '$date_now'")->row_array();
      if (@$id['no_id'] =='' || @$id['modul'] == '') {
        $result = date('ymd').'0001';
      }else{
        $result = $id['no_id']+1;
      }
      return $result;
    }
  }

  if (!function_exists('update_id')){
    function update_id($modul='', $no_id='')
    {
      $CI = get_instance();

      $date_now = date('Y-m-d');
      $check = $CI->db->query("SELECT * FROM tmp_id WHERE modul = '$modul'")->row_array();

      if (@$check['no_id'] == '') {
        $result = $CI->db->query("INSERT INTO tmp_id (modul, tgl_id, no_id) VALUES ('$modul', '$date_now', '$no_id')");
      }else{
        $result = $CI->db->query("UPDATE tmp_id SET tgl_id = '$date_now', no_id = '$no_id' WHERE modul = '$modul'");
      }

      return $result;
    }
  }

  if (!function_exists('get_file_type')){
    function get_file_type($file_name=null) {
      $arr = explode('.', $file_name);
      $len = count($arr)-1;
      $file_type = $arr[$len];
      return $file_type;
    }
  }

  if (!function_exists('create_title_img')){
    function create_title_img($path_dir, $tmp_name, $fupload_name='', $old_file=null, $name_file='') {
      if($old_file != "") {
          unlink($path_dir . $old_file);
      }
      //
      $file_type = get_file_type($fupload_name);
      if ($name_file !='') {
        $file_name = 'simrs-' . $name_file . '.' . $file_type;
      }else{
        $file_name = 'simrs-' . md5(md5(date('Y-m-d H:i:s').microtime().$fupload_name)). '.' . $file_type;
      }
      $vfile_upload = $path_dir . $file_name;
      //
      return @$file_name;
    }
  }

  if (!function_exists('assets_url')){
    function assets_url($path_dir='') {
      return './../simrs/assets/images/pendaftaran_online/'.$path_dir.'/';
    }
  }

  if (!function_exists('create_status')){
    function create_status($status='', $type='') {
      if ($type == 'full') {
        $result = md5(md5($status)).md5(md5(md5($status))).md5(md5(date('Ymd').microtime())); 
      }else{
        $result = md5(md5(md5($status)));
      }
      
      return $result;
    }
  }

  if (!function_exists('decrypt_status')){
    function decrypt_status($status='') {
      $var_1 = substr($status, 0, 32);
      $var_2 = substr($status, 32, 32);
      $var_3 = substr($status, 64, 32);

      return $var_2;
    }
  }

  if (!function_exists('get_parameter')){
    function get_parameter($value, $type = 'parameter_field') {
      $CI = get_instance();

      $res = $CI->db->query("SELECT * FROM mst_parameter WHERE $type = '$value'")->result_array();
      return $res;
    }
  }

  if (!function_exists('get_wilayah')){
    function get_wilayah($wilayah=null, $type=null) {
      $result = explode("#",$wilayah);
      if ($type == 'id') {
        return $result[0];
      }else{
        return $result[1];
      }
    }
  }

  if(!function_exists('to_date_indo')) {
    function to_date_indo($tgl='', $type='') {
      if($tgl != '') {
          $tanggal = substr($tgl, 8, 2);
          $jam = substr($tgl, 11, 8);
          $bulan = month(substr($tgl, 5, 2));
          $tahun = substr($tgl, 0, 4);
          if ($type == 'date') {
            return $tanggal . ' ' . $bulan . ' ' . $tahun;
          }elseif ($type == 'time') {
            if($jam != '') {
                return $jam . ' WIB';
            }
          }else{
            if($jam != '') {
                return $tanggal . ' ' . $bulan . ' ' . $tahun . ' ' . $jam . ' WIB';
            } else {
                return $tanggal . ' ' . $bulan . ' ' . $tahun;
            }    
          }
      }    
    }
  }

  if(!function_exists('month')) {
    function month($bln) {
      switch ($bln)
      {
        case 1:
          return "Januari";
          break;
        case 2:
          return "Februari";
          break;
        case 3:
          return "Maret";
          break;
        case 4:
          return "April";
          break;
        case 5:
          return "Mei";
          break;
        case 6:
          return "Juni";
          break;
        case 7:
          return "Juli";
          break;
        case 8:
          return "Agustus";
          break;
        case 9:
          return "September";
          break;
        case 10:
          return "Oktober";
          break;
        case 11:
          return "November";
          break;
        case 12:
          return "Desember";
          break;
      }
    }
  }

  if(!function_exists('day')) {
    function day($hari){
      switch($hari){
        case 'Sun':
          $day = "Minggu";
        break;
        case 'Mon':     
          $day = "Senin";
        break;
        case 'Tue':
          $day = "Selasa";
        break;   
        case 'Wed':
          $day = "Rabu";
        break;   
        case 'Thu':
          $day = "Kamis";
        break;   
        case 'Fri':
          $day = "Jumat";
        break;   
        case 'Sat':
          $day = "Sabtu";
        break;
        default:
          $day = "Tidak di ketahui";   
        break;
      }
      return $day;   
    }
  }