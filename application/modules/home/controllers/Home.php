<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	var $menu;

	function __construct(){
    parent::__construct();
		$this->load->model(array(
			'm_home',
		));

		$this->menu = 'home';
  }

	public function index()
	{
		$data['menu'] = $this->menu;
		$data['list_informasi'] = $this->m_home->list_informasi();
		$data['identitas']   = $this->m_app->_get_identitas();
	  $this->render('index',$data);
	}
  
}
