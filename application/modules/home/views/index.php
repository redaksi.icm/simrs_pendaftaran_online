<!-- register Section -->
<section class="page-section register" id="register" style="padding-top: 6.8rem; padding-right: 0px; padding-bottom: 4.5rem; padding-left: 0px;">
  <div class="container" >

    <!-- register Section Heading -->
    <h2 class="page-section-heading text-center text-secondary mb-0"><?=@$identitas['rumah_sakit']?></h2>
    <h3 class="text-center">Antrian Online <?=@$identitas['rumah_sakit']?></h3>

    <!-- Icon Divider -->
    <div class="divider-custom">
      <div class="divider-custom-line"></div>
      <div class="divider-custom-icon">
        <i class="fas fa-star"></i>
      </div>
      <div class="divider-custom-line"></div>
    </div>

    <!-- register Grid Items -->
    <div class="row">
      <div class="col-md-3 col-lg-4">
        <a class="js-scroll-trigger" href="#about">
          <div class="register-item mx-auto">
            <div class="register-item-caption-content text-center text-white">
              <i class="fas fa-file-alt fa-4x"></i>
            </div><br>
            <h3 class="text-center text-white text-uppercase">Informasi</h3>
          </div>
        </a>
      </div>
      <div class="col-md-3 col-lg-4">
        <a href="<?=site_url('jadwal_dokter')?>">
          <div class="register-item mx-auto">
            <div class="register-item-caption-content text-center text-white">
              <i class="fas fa-calendar-alt fa-4x"></i>
            </div><br>
            <h3 class="text-center text-white text-uppercase">Jadwal Dokter</h3>
          </div>
        </a>
      </div>
      <div class="col-md-3 col-lg-4">
        <a href="<?=site_url('pendaftaran')?>">
          <div class="register-item mx-auto">
            <div class="register-item-caption-content text-center text-white">
              <i class="fas fa-file-contract fa-4x"></i>
            </div><br>
            <h3 class="text-center text-white text-uppercase">Pendaftaran</h3>
          </div>
        </a>
      </div>
    </div>
    <!-- /.row -->

  </div>
</section>

<!-- About Section -->
<section class="page-section bg-primary text-white mb-0" id="about" style="padding-top: 3.2rem; padding-right: 0px; padding-bottom: 3.2rem; padding-left: 0px;">
  <div class="container">

    <!-- About Section Heading -->
    <h2 class="page-section-heading text-center text-uppercase text-white">INFORMASI</h2>

    <!-- Icon Divider -->
    <div class="divider-custom divider-light">
      <div class="divider-custom-line"></div>
      <div class="divider-custom-icon">
        <i class="fas fa-star"></i>
      </div>
      <div class="divider-custom-line"></div>
    </div>

    <!-- About Section Content -->
    <div class="row">
      <?php foreach ($list_informasi as $informasi): ?>
      <div class="col-lg-4 ml-auto">
        <p class="lead"><?=$informasi['info_txt']?></p>
      </div>
      <?php endforeach; ?>
    </div>

  </div>
</section>

<section class="page-section text-white mb-0" style="padding-top: 3.2rem; padding-right: 0px; padding-bottom: 3.2rem; padding-left: 0px;">
</section>