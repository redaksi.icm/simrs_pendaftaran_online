<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_home extends CI_Model {

  public function list_informasi()
  {
    $sql = "SELECT * FROM web_info_antrian WHERE is_deleted = 0 AND is_active = 1";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}