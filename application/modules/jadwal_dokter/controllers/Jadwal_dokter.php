<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_dokter extends MY_Controller {

	var $menu;

	function __construct(){
    parent::__construct();
		$this->load->model(array(
			'm_jadwal_dokter',
		));

		$this->menu = 'jadwal_dokter';
  }

	public function index()
	{
		$data['menu'] = $this->menu;
		$data['main'] = $this->m_jadwal_dokter->all_data();
	  $this->render('index',$data);
	}
  
}
