<!-- register Section -->
<section class="page-section register" id="register" style="padding: 8rem 0;">
  <div class="container" >

    <!-- register Section Heading -->
    <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Jadwal Dokter</h2>

    <!-- Icon Divider -->
    <div class="divider-custom">
      <div class="divider-custom-line"></div>
      <div class="divider-custom-icon">
        <i class="fas fa-star"></i>
      </div>
      <div class="divider-custom-line"></div>
    </div>

    <!-- Contact Section Form -->
    <div class="table-responsive">
      <table class="table table-hover table-striped table-bordered table-fixed">
        <thead>
          <tr>
            <th class="text-center" width="36">No</th>
            <th class="text-center">Lokasi</th>
            <th class="text-center" width="150">Senin</th>
            <th class="text-center" width="150">Selasa</th>
            <th class="text-center" width="150">Rabu</th>
            <th class="text-center" width="150">Kamis</th>
            <th class="text-center" width="150">Jum'at</th>
            <th class="text-center" width="150">Sabtu</th>
            <th class="text-center" width="150">Minggu</th>
          </tr>
        </thead>
        <?php if(@$main == null):?>
          <tbody>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else:?>
          <tbody>
            <?php $i=1;foreach($main as $row):?>
              <tr>
                <td class="text-center text-middle" width="36"><?=($i++)?></td>
                <td><?=$row['lokasi_nm']?></td>
                <td class="text-center text-top" width="150">
                  <?php if (count($row['rinc_senin']) > 1): ?>
                    <?php foreach ($row['rinc_senin'] as $senin): ?>
                      <b><?=$senin['pegawai_nm']?></b><br><?=$senin['jam']?><hr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </td>
                <td class="text-center text-top" width="150">
                  <?php if (count($row['rinc_selasa']) > 1): ?>
                    <?php foreach ($row['rinc_selasa'] as $selasa): ?>
                      <b><?=$selasa['pegawai_nm']?></b><br><?=$selasa['jam']?><hr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </td>
                <td class="text-center text-top" width="150">
                  <?php if (count($row['rinc_rabu']) > 1): ?>
                    <?php foreach ($row['rinc_rabu'] as $rabu): ?>
                      <b><?=$rabu['pegawai_nm']?></b><br><?=$rabu['jam']?><hr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </td>
                <td class="text-center text-top" width="150">
                  <?php if (count($row['rinc_kamis']) > 1): ?>
                    <?php foreach ($row['rinc_kamis'] as $kamis): ?>
                      <b><?=$kamis['pegawai_nm']?></b><br><?=$kamis['jam']?><hr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </td>
                <td class="text-center text-top" width="150">
                  <?php if (count($row['rinc_jumat']) > 1): ?>
                    <?php foreach ($row['rinc_jumat'] as $jumat): ?>
                      <b><?=$jumat['pegawai_nm']?></b><br><?=$jumat['jam']?><hr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </td>
                <td class="text-center text-top" width="150">
                  <?php if (count($row['rinc_sabtu']) > 1): ?>
                    <?php foreach ($row['rinc_sabtu'] as $sabtu): ?>
                      <b><?=$sabtu['pegawai_nm']?></b><br><?=$sabtu['jam']?><hr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </td>
                <td class="text-center text-top" width="150">
                  <?php if (count($row['rinc_minggu']) > 1): ?>
                    <?php foreach ($row['rinc_minggu'] as $minggu): ?>
                      <b><?=$minggu['pegawai_nm']?></b><br><?=$minggu['jam']?><hr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </td>
              </tr>
            <?php endforeach;?>
          </tbody>
        <?php endif;?>
      </table>
    </div>
    <!-- End Contact Section Form -->
  </div>
</section>