<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jadwal_dokter extends CI_Model {

  public function all_data()
  {
    $sql = "SELECT 
              a.*, b.lokasi_nm
            FROM web_jadwal_dokter a
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    // 
    foreach($result as $key => $val) {
      $result[$key]['rinc_senin'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'senin');
      $result[$key]['rinc_selasa'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'selasa');
      $result[$key]['rinc_rabu'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'rabu');
      $result[$key]['rinc_kamis'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'kamis');
      $result[$key]['rinc_jumat'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'jumat');
      $result[$key]['rinc_sabtu'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'sabtu');
      $result[$key]['rinc_minggu'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'minggu');
    }
    return $result;
  }

  public function jadwal_dokter_rinc($jadwal_id='', $hari='')
  {
    $sql = "SELECT a.*, b.pegawai_nm 
            FROM web_jadwal_dokter_rinc a 
            LEFT JOIN mst_pegawai b ON a.dokter_id=b.pegawai_id
            WHERE a.is_deleted = 0 
            AND a.jadwal_id=?
            AND a.hari=? 
            ORDER BY created_at";
    $query = $this->db->query($sql, array($jadwal_id, $hari));
    return $query->result_array();
  }
  
}