<!-- js -->
<?php $this->load->view('pendaftaran/_js')?>
<!-- / -->
<!-- register Section -->
<section class="page-section register" id="register" style="padding: 8rem 0;">
  <div class="container" >

    <!-- register Section Heading -->
    <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Form Pendaftaran Online</h2>

    <!-- Icon Divider -->
    <div class="divider-custom">
      <div class="divider-custom-line"></div>
      <div class="divider-custom-icon">
        <i class="fas fa-star"></i>
      </div>
      <div class="divider-custom-line"></div>
    </div>

    <!-- Contact Section Form -->
    <form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
      <div class="row">

        <div class="col-lg-6">
          <div class="control-group">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-6">
                  <label>Memiliki Nomor Rekam Medis ?</label>
                  <div>
                    <input type="radio" id="rm-ya" name="rm" value="1">
                    <label for="rm-ya" class="input-radio">Ya</label>
                    <span class="pr-5"></span>
                    <input type="radio" id="rm-tidak" name="rm" value="2">
                    <label for="rm-tidak" class="input-radio">Tidak</label>
                  </div>
                </div>
                <div class="col-lg-6 d-none" id="no-rm">
                  <label>No Rekam Medis (RM)</label>
                  <input class="form-control mb-n2" type="text" name="pasien_id" id="pasien_id" placeholder="Masukan No Rekam Medis (RM)" required="">
                  <div class="small-text text-success mt-2 ml-1 mb-n4 d-none" id="no-rm-used">No. Rekam Medis sudah ada</div>
                  <i class="fas fa-spinner fa-pulse font-loading-input mt-n3 d-none" id="pasien_id_loading"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-9">
                  <label>NIK</label>
                  <div class="row">
                    <div class="col-lg-8">
                      <input class="form-control" type="text" name="nik" id="nik" placeholder="Masukan NIK" required="">
                      <i class="fas fa-spinner fa-pulse font-loading-input d-none" id="nik_loading"></i>
                    </div>
                    <div class="col-lg-4">
                      <button type="button" class="btn btn-sm btn-primary btn-search-nik" id="btn-nik"><i class="fas fa-search"></i> Cari NIK</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <label>Nama Pasien</label>
              <input class="form-control" type="text" name="pasien_nm" id="pasien_nm" placeholder="Masukan Nama" required="">
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-6">
                  <label>Jenis Kelamin</label>
                  <select class="form-control select2" name="sex_cd" id="sex_cd" required="">
                    <option value="">-- Pilih --</option>
                    <?php foreach(get_parameter('sex_cd') as $r): ?>
                      <option value="<?=$r['parameter_cd']?>"><?=$r['parameter_val']?></option>
                    <?php endforeach;?>
                  </select>
                </div>
                <div class="col-lg-6">
                  <label>No Telepon</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">+62</span>
                    </div>
                    <input class="form-control" type="number" name="no_telp" id="no_telp" placeholder="Masukan No Telepon" required="">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-6">
                  <label>Tempat Lahir</label>
                  <input class="form-control" type="text" name="tmp_lahir" id="tmp_lahir" placeholder="Masukan Tempat Lahir" required="">
                </div>
                <div class="col-lg-6">
                  <label>Tanggal Lahir</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                    </div>
                    <input class="form-control datepicker" type="text" name="tgl_lahir" id="tgl_lahir" placeholder="Masukan Tanggal Lahir" required="">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-6">
                  <label>Tanggal Periksa</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                    </div>
                    <input class="form-control datemax" name="tgl_periksa" type="text" placeholder="Pilih Tanggal Periksa" required="">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <label>Klinik Tujuan</label>
              <select class="form-control select2" name="lokasi_id" id="lokasi_id" required="">
                <option value="">-- Pilih --</option>
                <?php foreach ($list_klinik_tujuan as $klinik): ?>
                <option value="<?=$klinik['lokasi_id']?>"><?=$klinik['lokasi_nm']?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-6">
                  <label>Jenis Pasien</label>
                  <select class="form-control select2" name="jenispasien_id" id="jenispasien_id" required="">
                    <option value="">-- Pilih --</option>
                    <?php foreach ($list_jenis_pasien as $pasien): ?>
                    <option value="<?=$pasien['jenispasien_id']?>"><?=$pasien['jenispasien_nm']?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-6 d-none" id="no-kartu">
                  <label>No. Kartu</label>
                  <input class="form-control" type="text" name="no_kartu" placeholder="Masukan Nomor Kartu" required="">
                </div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <label>Memiliki Surat Rujukan ?</label>
              <div>
                <input tabindex="11" type="radio" id="sr-ya" name="is_have_rujukan" value="1">
                <label for="sr-ya" class="input-radio">Ya</label>
                <span class="pr-5"></span>
                <input tabindex="12" type="radio" id="sr-tidak" name="is_have_rujukan" value="0">
                <label for="sr-tidak" class="input-radio">Tidak</label>
              </div>
            </div>
          </div>
          <div class="control-group d-none" id="upload-ektp">
            <div class="form-group">
              <label>Upload Foto EKTP/Sep Terakhir</label>
              <input class="form-control" type="file" name="file_ktp" style="padding-top: 3px!important;">
              <small class="text-danger">*) Format File .png .jpg .jpeg</small>
            </div>
          </div>
          <div class="control-group d-none" id="upload-surat-rujuk">
            <div class="form-group">
              <label>Upload Foto Surat Rujukan</label>
              <input class="form-control" type="file" name="file_rujukan" style="padding-top: 3px!important;">
              <small class="text-danger">*) Format File .png .jpg .jpeg</small>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="control-group">
            <div class="form-group">
              <label>Wilayah</label>
              <select class="form-control select2" name="wilayah_st" id="wilayah_st" required="">
                <option value="">-- Pilih --</option>
                <option value="D">Dalam Wilayah</option>
                <option value="L">Luar Wilayah</option>
              </select>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-6">
                  <label>Provinsi <small class="text-danger">(wilayah harus diisi)</small></label>
                  <div id="box_wilayah_prop">
                    <select class="form-control select2" name="wilayah_prop" id="wilayah_prop" required="">
                      <option value="">-- Pilih --</option>
                    </select>
                  </div>
                </div>
                <div class="col-lg-6">
                  <label>Kabupaten/Kota</label>
                  <div id="box_wilayah_kab">
                    <select class="form-control select2" name="wilayah_kab" id="wilayah_kab" required="">
                      <option value="">-- Pilih --</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-6">
                  <label>Kecamatan</label>
                  <div id="box_wilayah_kec">
                    <select class="form-control select2" name="wilayah_kec" id="wilayah_kec" required="">
                      <option value="">-- Pilih --</option>
                    </select>
                  </div>
                </div>
                <div class="col-lg-6">
                  <label>Desa/Kelurahan</label>
                  <div id="box_wilayah_kel">
                    <select class="form-control select2" name="wilayah_kel" id="wilayah_kel" required="">
                      <option value="">-- Pilih --</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <label>Alamat Lengkap</label>
              <textarea class="form-control" rows="4" name="alamat" id="alamat" placeholder="Masukan Alamat Lengkap"></textarea>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-4">
                  <div class="form-control captcha-validate" id="captcha-img"><?=captcha()?></div>
                  <input type="hidden" name="captcha_validate" id="captcha_validate">
                </div>
                <div class="col-lg-5">
                  <input class="form-control" type="text" name="captcha" id="captcha" placeholder="Isikan Kode Disamping" required="" style="height: 40px !important;">
                </div>
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block btn-md btn-submit mt-2 font-weight-bold">
                SELESAI <i class="fas fa-arrow-right ml-1"></i>
              </button>
            </div>
          </div>
        </div>
        
      </div>
    </form>
  </div>
</section>