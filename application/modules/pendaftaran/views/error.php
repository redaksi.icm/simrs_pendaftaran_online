<!-- register Section -->
<section class="page-section register" id="register" style="padding-top: 8rem; min-height: 100vh; margin-bottom: -73px;">
  <div class="container" >

    <!-- register Section Heading -->
    <h2 class="page-section-heading text-center text-uppercase text-danger mb-0">Pendaftaran Gagal</h2>

    <!-- Icon Divider -->
    <div class="divider-custom">
      <div class="divider-custom-line"></div>
      <div class="divider-custom-icon">
        <i class="fas fa-times"></i>
      </div>
      <div class="divider-custom-line"></div>
    </div>

    <!-- Contact Section Form -->
    <div class="text-center">
      <?php if ($decrypt_status == create_status('error_img')): ?>
        <p class="font-weight-bold mb-0" style="font-size: 1.5rem;">- Format Gambar Salah -</p>
        <p class="font-weight-light mb-0" style="font-size: 1.2rem;">Silahkan upload gambar dengan format yang benar</p>
      <?php elseif ($decrypt_status == create_status('limit_reg')): ?>
        <p class="font-weight-bold mb-0" style="font-size: 1.5rem;">- Tidak Bisa Daftar Lagi -</p>
        <p class="font-weight-light mb-0" style="font-size: 1.2rem;">Anda sudah pernah mendaftar di pendaftaran online ini</p>
      <?php elseif ($decrypt_status == create_status('max_pasien')): ?>
        <p class="font-weight-bold mb-0" style="font-size: 1.5rem;">- Tidak Bisa Daftar Lagi -</p>
        <p class="font-weight-light mb-0" style="font-size: 1.2rem;">Klinik Tujuan yang Anda pilih sudah penuh</p>
      <?php endif; ?>
    </div>
    <!-- End Contact Section Form -->
  </div>
</section>