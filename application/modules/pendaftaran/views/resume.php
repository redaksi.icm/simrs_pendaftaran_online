<!-- register Section -->
<section class="page-section register" id="register" style="padding: 5.8rem 0;">
  <div class="container" style="max-width: 850px;">

    <!-- register Section Heading -->
    <h2 class="page-section-heading text-center text-uppercase text-secondary mb-n3" style="font-size: 2.5rem;">Resume Pendaftaran Online</h2>

    <!-- Icon Divider -->
    <div class="divider-custom">
      <div class="divider-custom-line"></div>
      <div class="divider-custom-icon">
        <i class="fas fa-star"></i>
      </div>
      <div class="divider-custom-line"></div>
    </div>

    <p class="font-weight-light text-center mb-3 mt-n4" style="font-size: 1.5rem">Harap simpan atau print data dibawah untuk ditunjukan ke petugas sebagai bukti telah mendaftar</p>

    <!-- Contact Section Form -->
    <div class="table-responsive">
      <table class="table table-bordered table-sm">
        <tbody>
          <tr>
            <th class="text-left" width="33%">Nama</th>
            <td class="text-left"><?=@$resume['pasien_nm']?></td>
          </tr>
          <tr>
            <th class="text-left" width="33%">No. Antrian</th>
            <td class="text-left"><?=@$resume['antrian_no']?></td>
          </tr>
          <tr>
            <th class="text-left" width="33%">Tanggal - Jam</th>
            <td class="text-left"><?=to_date_indo(@$resume['tgl_periksa'])?></td>
          </tr>
          <tr>
            <th class="text-left" width="33%">Jenis Pasien</th>
            <td class="text-left"><?=@$resume['jenispasien_nm']?></td>
          </tr>
          <tr>
            <th class="text-left" width="33%">Poli</th>
            <td class="text-left"><?=@$resume['lokasi_nm']?></td>
          </tr>
          <tr>
            <th class="text-left" width="33%">Alamat</th>
            <td class="text-left"><?=@$resume['alamat']?>, <?=@$resume['kelurahan']?>, <?=@$resume['kecamatan']?>, <?=@$resume['kabupaten']?>, <?=@$resume['provinsi']?></td>
          </tr>
          <tr>
            <th class="text-left" width="33%">ID</th>
            <td class="text-left"><?=@$resume['regonline_id']?></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="text-center mt-n3">
      <img src="<?=base_url().'images/qrcode/qrcode.png'?>" style="width: 180px;">
    </div>
    <h4 class="text-center pt-2">Terimakasih Telah Mendaftar di <?=@$identitas['rumah_sakit']?></h4>
    <div class="text-center pt-2">
      <p>Anda telah terdaftar untuk antrian pendaftaran online pada <br> Tanggal : <b><?=to_date_indo(@$resume['tgl_periksa'], 'date')?></b> dan waktu perkiraan akan dilayani di Loket Pendaftaran Online pada <br> pukul : <b><?=to_date_indo(@$resume['tgl_periksa'], 'time')?></b>. Dimohon datang sebelum waktunya. Nomor antrian tidak dapat digunakan <br> apabila dantang 15 menit melebihi dari waktu perkiraan pelayanan di Loket Pendaftaran Online. <br><br></p>
    </div>
    <div class="text-center mt-n4">
      <p><b>Silahkan Screenshot (dibagian tabel deskripsi & QRCode) atau Download Bukti PDF di bawah ini</b></p>
    </div>
    <div class="text-center">
      <a href="<?=site_url('pendaftaran/download_bukti_pdf/'.$id)?>" target="_blank" class="btn btn-info"><i class="fas fa-download"></i> DOWNLOAD BUKTI PDF</a>
    </div>
    <!-- End Contact Section Form -->
  </div>
</section>