<script type="text/javascript">
$(function() {
  $(document).ready(function () {
    var form = $("#form-data").validate( {
      rules: {
        captcha:{
          remote: {
            url: '<?=site_url()?>/pendaftaran/ajax/cek_captcha',
            type: 'post',
            data: {
              captcha_validate: $("#captcha-img").html(),
              captcha: function() {
                return $("#captcha").val();
              }
            }
          }
        },
      },
      messages: {
        captcha : {
          remote : 'Kode tidak sama !'
        }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('select2')){
          error.insertAfter(element.next(".select2-container"));
        } else if($(element).hasClass('iradio_square-green')){
          error.insertAfter(element.next(".iradio_square-green"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> PROSES');
        $(".btn-submit").attr("disabled", "disabled");
        form.submit();
      }
    });
    //
    $('.datepicker').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
    //
    $('.datemax').daterangepicker({
      minDate: '<?=$limit_tgl['min_date']?>',
      maxDate: '<?=$limit_tgl['max_date']?>',
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
    //
    $('#tgl_lahir').val('');
    //
    var typing_timer;
    $('#pasien_id').on('keyup', function () {
      clearTimeout(typing_timer);
      typing_timer = setTimeout(typing_done, 700);
    });

    $('#pasien_id').on('keydown', function () {
      clearTimeout(typing_timer);
    });

    function typing_done() {
      var id = $('#pasien_id').val();
      // $('#no-rm-used').addClass('d-none');
      no_rm_fill(id);
    }
    //
    $('#wilayah_st').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_provinsi(i);
    })
    function _get_provinsi(i) {
      $.get('<?=site_url('pendaftaran/ajax/get_provinsi_by_st')?>?wilayah_st='+i,null,function(data) {
          $('#box_wilayah_prop').html(data.html);
      },'json');
    }
    function _empty_kabupaten() {
      $.get('<?=site_url('pendaftaran/ajax/empty_kabupaten')?>',null,function(data) {
          $('#box_wilayah_kab').html(data.html);
      },'json');
    }

    function _empty_kecamatan() {
      $.get('<?=site_url('pendaftaran/ajax/empty_kecamatan')?>',null,function(data) {
          $('#box_wilayah_kec').html(data.html);
      },'json');
    }

    function _empty_kelurahan() {
      $.get('<?=site_url('pendaftaran/ajax/empty_kelurahan')?>',null,function(data) {
          $('#box_wilayah_kel').html(data.html);
      },'json');
    }

    function _get_wilayah(i, prop='', kab='', kec='', kel='') {
      var ext_var = 'wilayah_prop='+prop;
          ext_var+= '&wilayah_kab='+kab;
          ext_var+= '&wilayah_kec='+kec;
          ext_var+= '&wilayah_kel='+kel;
      $.get('<?=site_url('pendaftaran/ajax/get_wilayah_id_name')?>?wilayah_parent='+i+'&'+ext_var,null,function(data) {
          $('#box_wilayah_kab').html(data.html);
      },'json');
    }
    //
    $("input[name='rm']").on("ifChecked", function() {
      var val = $(this).val();
      if (val == 1) {
        $('#no-rm').removeClass('d-none');
      } else {
        $('#no-rm').addClass('d-none');
      }
    });
    //
    $("input[name='is_have_rujukan']").on("ifChecked", function() {
      var val = $(this).val();
      if (val == 1) {
        $('#upload-ektp').removeClass('d-none');
        $('#upload-surat-rujuk').removeClass('d-none');
      } else {
        $('#upload-ektp').addClass('d-none');
        $('#upload-surat-rujuk').addClass('d-none');
      }
    });
    //
    var captcha_img = $('#captcha-img').text();
    $('#captcha_validate').val(captcha_img);
    //
    $('#jenispasien_id').bind('change',function(e) {
      e.preventDefault();
      var val = $(this).val();
      if (val == '02') {
        $('#no-kartu').removeClass('d-none');
      }else{
        $('#no-kartu').addClass('d-none');
      }
    })
    //
    $(".select2").select2();
    $('.select2-container').css('width', '100%');
    $('.select2').on('select2:select', function (e) {
      form.element("#lokasi_id");
      form.element("#jenispasien_id");
      form.element("#wilayah_st");
      form.element("#wilayah_prop");
      form.element("#wilayah_kab");
      form.element("#wilayah_kec");
      form.element("#wilayah_kel");
    });

    function zero_fill(str, max) {
      str = str.toString();
      return str.length < max ? zero_fill("0" + str, max) : str;
    }

    function ucfirst(str) {
      str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
          return letter.toUpperCase();
      });
      return str;
    }

    $('#btn-nik').on('click',function(e) {
      e.preventDefault();
      $("#nik_loading").removeClass('d-none');
      $("#btn-nik").html('<i class="fas fa-spin fa-spinner"></i> Proses');
      $("#btn-nik").attr("disabled", "disabled");
      var nik = $("#nik").val();
      if (!Number.isInteger(parseInt(nik))) {
        Swal.fire({
          title: 'NIK harus berupa angka!',
          type: 'warning',
          customClass: 'swal-wide'
        })
        $("#nik_loading").addClass('d-none');
        $("#btn-nik").html('<i class="fas fa-search"></i> Cari NIK');
        $("#btn-nik").removeAttr("disabled");
      }else{
        $.ajax({
          type: "POST",
          url: "<?=site_url()?>/pendaftaran/token",
          data : {nik: nik, a: b('_t'), b: b('_token'), c: b('_trash'), d: b('_validate')},
          dataType: "json",
          success: function (data) {
            console.log(data);
            if (data.STATUS == 0) {
              Swal.fire({
                title: 'Data tidak ditemukan',
                type: 'warning',
                customClass: 'swal-wide'
              })
              $("#pasien_nm").val(data.NAMA_LGKP);
              $("#sex_cd").val('').trigger('change');
              $("#tmp_lahir").val('');
              $("#tgl_lahir").val('');
              $("#alamat").val('');
              $("#wilayah_st").val('').trigger('change');
              _get_provinsi(wilayah_st);
              setTimeout(function(){ 
                $("#wilayah_prop").val('').trigger('change');
              }, 800);
              setTimeout(function(){ 
                _empty_kabupaten();
                _empty_kecamatan();
                _empty_kelurahan();
              }, 900);
            }else if(data.STATUS == -1){
              Swal.fire({
                title: 'Terjadi Kesalahan Server!',
                text: "Silahkan ulangi kembali.",
                type: 'warning',
                customClass: 'swal-wide'
              })
            }else if(data.STATUS == -2){
              Swal.fire({
                title: 'Terjadi Kesalahan Server!',
                text: "Silahkan refresh halaman ini.",
                type: 'warning',
                customClass: 'swal-wide'
              })
            }else if(data.STATUS == 1){
              $("#pasien_nm").val(data.NAMA_LGKP);
              if (data.JENIS_KLMIN === 'LAKI-LAKI') {
                $("#sex_cd").val('L').trigger('change');
              }else{
                $("#sex_cd").val('P').trigger('change');
              }
              $("#tmp_lahir").val(data.TMPT_LHR);
              var tgl = data.TGL_LHR.split(".");
              $("#tgl_lahir").val(tgl[0]+'-'+tgl[1]+'-'+tgl[2]);
              var alamat = data.ALAMAT+' RT. '+data.NO_RT+' RW. ';
              if (data.NO_RW == null) {
                alamat += '- ';
              }else{
                alamat += data.NO_RW+' ';
              };
              if (data.DUSUN !='-' && data.DUSUN !=null) {
                alamat += 'Dsn. '+data.DUSUN;  
              }
              $("#alamat").val(alamat);
              $("#no_kk").val(data.NO_KK);
              $("#kode_pos").val(data.KODE_POS);
              var prop = zero_fill(data.NO_PROP,2);
              var kab = prop+'.'+zero_fill(data.NO_KAB,2);
              var kec = kab+'.'+zero_fill(data.NO_KEC,2);
              var kel = kec+'.'+zero_fill(data.NO_KEL,4);
              var wilayah_id = zero_fill(data.NO_PROP,2)+'.'+zero_fill(data.NO_KAB,2)+'.'+zero_fill(data.NO_KEC,2)+'.'+zero_fill(data.NO_KEL,4);
              if (prop != '33') {
                var wilayah_st = 'L';
              }else{
                var wilayah_st = 'D';
              }
              $("#wilayah_st").val(wilayah_st).trigger('change');
              _get_provinsi(wilayah_st);
              if (wilayah_id !='') {
                var wil = wilayah_id.split(".");
              }
              setTimeout(function(){ 
                if (wilayah_id !='') {
                  $("#wilayah_prop").val(wil[0]+"#"+data.PROP_NAME).trigger('change');
                }else{
                  $("#wilayah_prop").val('').trigger('change');
                }
              }, 700);
              setTimeout(function(){ 
                if (wilayah_id !='') {
                  _get_wilayah(wil[0], wil[0], wil[0]+'.'+wil[1], wil[0]+'.'+wil[1]+'.'+wil[2], wilayah_id);
                }else{
                  _empty_kabupaten();
                  _empty_kecamatan();
                  _empty_kelurahan();
                }
              }, 1000);
            }
            $("#nik_loading").addClass('d-none');
            $("#btn-nik").html('<i class="fas fa-search"></i> Cari NIK');
            $("#btn-nik").removeAttr("disabled");
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
          }
        });
      }
    })
  
    function no_rm_fill(id, pasien_id='') {
      $("#pasien_id_loading").removeClass('d-none');
      $.ajax({
        type : 'post',
        url : '<?=site_url('pendaftaran/ajax/no_rm_fill')?>',
        dataType : 'json',
        data : 'pasien_id='+id,
        success : function (data) {
          if (data.status_cari == 1) {
            fill_form(data, pasien_id);
          }else{
            reset_form();
          }
          $("#pasien_id_loading").addClass('d-none');
        }
      })
    }

    function fill_form(data, pasien_id='') {
      $('#no-rm-used').removeClass('d-none');
      if (pasien_id == 'true') {
        $("#pasien_id").val(data.pasien_id);
      }
      $("#nik").val(data.nik);
      $("#pasien_nm").val(data.pasien_nm);
      $("#alamat").val(data.alamat);
      $("#wilayah_st").val(data.wilayah_st);
      _get_provinsi(data.wilayah_st);
      if (data.wilayah_id !='') {
        var wil = data.wilayah_id.split(".");
      }
      setTimeout(function(){ 
        if (data.wilayah_id !='') {
          $("#wilayah_prop").val(wil[0]+"#"+data.provinsi).trigger('change');
        }else{
          $("#wilayah_prop").val('').trigger('change');
        }
      }, 200);
      setTimeout(function(){ 
        if (data.wilayah_id !='') {
          _get_wilayah(wil[0], wil[0], wil[0]+'.'+wil[1], wil[0]+'.'+wil[1]+'.'+wil[2], data.wilayah_id);
        }else{
          _empty_kabupaten();
          _empty_kecamatan();
          _empty_kelurahan();
        }
      }, 400);
      $("#tmp_lahir").val(data.tmp_lahir);
      $("#tgl_lahir").val(to_date(data.tgl_lahir));
      $("#sex_cd").val(data.sex_cd).trigger('change');
      $("#no_telp").val(data.no_telp);
      $("#jenispasien_id").val(data.jenispasien_id).trigger('change');
      $("#no_kartu").val(data.no_kartu);
      $("#pasien_id_loading").addClass('d-none');
    }  

    function reset_form() {
      $('#no-rm-used').addClass('d-none');
      $("#nik").val('');
      $("#pasien_nm").val('');
      $("#nama_kk").val('');
      $("#alamat").val('');
      $("#wilayah_st").val('').trigger('change');
      _empty_kabupaten();
      _empty_kecamatan();
      _empty_kelurahan();
      $("#tmp_lahir").val('');
      $("#tgl_lahir").val('');
      $("#no_telp").val('');
      $("#jenispasien_id").val('').trigger('change');
    }

  })
});
</script>