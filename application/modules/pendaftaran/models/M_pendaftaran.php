<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pendaftaran extends CI_Model {

  public function get_profile()
  {
    return $this->db->get('app_config')->row_array();
  }

  public function get_identitas()
  {
    return $this->db->get('mst_identitas')->row_array();
  }

  public function list_klinik_tujuan()
  {
    $sql = "SELECT * FROM mst_lokasi WHERE is_reg_online = 1";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function list_jenis_pasien()
  {
    $sql = "SELECT * FROM mst_jenis_pasien";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function get_parameter($parameter_group='', $parameter_nm='') {
    $sql = "SELECT * FROM mst_parameter WHERE parameter_group=? AND parameter_nm=?";
    $query = $this->db->query($sql, array($parameter_group, $parameter_nm));
    return $query->row_array();
  }

  public function limit_tgl() {
    $get_parameter = $this->get_parameter('REGISTRASI', 'LIMIT TGL');
    $result['min_date'] = date('d-m-Y', strtotime(date('d-m-Y'). ' +1 days'));
    $result['max_date'] = date('d-m-Y', strtotime(date('d-m-Y'). $get_parameter['parameter_val'] .' days'));

    return $result;
  }

  public function list_wilayah_by_id($wilayah_id=null) {
    $sql = "SELECT * FROM mst_wilayah WHERE wilayah_id=? ORDER BY wilayah_id ASC";
    $query = $this->db->query($sql, $wilayah_id);
    return $query->result_array();
  }

  public function list_wilayah_by_parent($wilayah_parent=null) {
    $sql = "SELECT * FROM mst_wilayah WHERE wilayah_parent=? ORDER BY wilayah_id ASC";
    $query = $this->db->query($sql, $wilayah_parent);
    return $query->result_array();
  }

  public function get_params($wilayah_parent=null) {
    $params = array(
        'level' => $this->get_wilayah_level($wilayah_parent),    
        'level_nm' => $this->get_wilayah_level_nm($wilayah_parent),  
        'input_nm' => $this->get_wilayah_input_nm($wilayah_parent),  
        'wilayah_parent' => $wilayah_parent,
        'wilayah_back' => $this->get_wilayah_back($wilayah_parent),
    );
    $params['input_nm_next'] = $this->get_wilayah_input_nm('',($params['level']+1));
    return $params;
  }

  public function get_wilayah_level($wilayah_parent=null) {
    $len = strlen($wilayah_parent);
    if($len == 0 || $len == 1) $level = '1';
    elseif($len == 2) $level = '2';
    elseif($len == 5) $level = '3';
    elseif($len == 8) $level = '4';
    return @$level;
  }

  public function get_wilayah_level_nm($wilayah_parent=null) {
    $level = $this->get_wilayah_level($wilayah_parent);
    //
    $result = '';
    if($level == '1') $result = 'Propinsi';
    elseif($level == '2') $result = 'Kab/Kota';
    elseif($level == '3') $result = 'Kecamatan';
    elseif($level == '4') $result = 'Desa/Kelurahan';
    return $result;
  }

  public function get_wilayah_input_nm($wilayah_parent=null,$level=null) {
    if($level == '') $level = $this->get_wilayah_level($wilayah_parent);
    //
    $result = '';
    if($level == '1') $result = 'wilayah_prop';
    elseif($level == '2') $result = 'wilayah_kab';
    elseif($level == '3') $result = 'wilayah_kec';
    elseif($level == '4') $result = 'wilayah_kel';
    return $result;
  }

  public function get_wilayah_back($wilayah_parent=null) {
    $level = $this->get_wilayah_level($wilayah_parent);
    //
    $arr = explode('.', $wilayah_parent);
    //
    $result = '';
    if($level == '4') $result = $arr[0].'.'.$arr[1];
    elseif($level == '3') $result = $arr[0];
    return $result;
  }

  public function get_replace_id($id=null) {
    $var_1 = substr($id, 0, 32);
    $var_2 = substr($id, 32, 32);
    $var_3 = substr($id, 64, 32);
    //
    return $var_2;
  }

  public function is_reg_pasien_online($regonline_id=null) {
    $sql = "SELECT regonline_id FROM reg_pasien_online WHERE MD5(MD5(regonline_id))=?";
    $query = $this->db->query($sql, $regonline_id);
    if($query->num_rows() > 0) {
      $row = $query->row_array();
      return $row['regonline_id'];
    } else {
      return false;
    }
  }

  public function get_resume($id=null) {
    $sql = "SELECT 
    					a.*, b.jenispasien_nm, c.lokasi_nm 
    				FROM 
    					reg_pasien_online a 
    				LEFT JOIN mst_jenis_pasien b ON a.jenispasien_id = b.jenispasien_id
    				LEFT JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
    				WHERE regonline_id=?";
    $query = $this->db->query($sql, $id);
    return $query->row_array();
  }

  public function get_mst_lokasi($lokasi_id=null) {
    $sql = "SELECT 
              a.*
            FROM 
              mst_lokasi a 
            WHERE lokasi_id=?";
    $query = $this->db->query($sql, $lokasi_id);
    return $query->row_array();
  }

  public function get_lkt_antrian($lokasi_id=null, $tgl_antrian=null) {
    $sql = "SELECT 
              a.*
            FROM 
              lkt_antrian a 
            WHERE a.lokasi_id=? AND a.tgl_antrian=?";
    $query = $this->db->query($sql, array($lokasi_id, $tgl_antrian));
    return $query->row_array();
  }

  public function get_jadwal_dokter($lokasi_id=null, $hari=null) {
    $sql = "SELECT 
              a.jadwal_id, b.jam, b.jeda_waktu, b.max_pasien 
            FROM 
              web_jadwal_dokter a 
            LEFT JOIN web_jadwal_dokter_rinc b ON a.jadwal_id=b.jadwal_id AND b.hari='$hari'
            WHERE a.lokasi_id=?";
    $query = $this->db->query($sql, $lokasi_id);
    $row = $query->row_array();
    $expld_jam = explode(' - ', $row['jam']);
    $row['jam_buka'] = $expld_jam[0];
    $row['jam_tutup'] = $expld_jam[1];
    return $row;
  }

  public function get_reg_pasien_online($lokasi_id=null, $tgl_periksa=null) {
    $sql = "SELECT 
              a.regonline_id, TIME(a.tgl_periksa) AS waktu_periksa, a.antrian_no 
            FROM 
              reg_pasien_online a 
            WHERE a.lokasi_id='$lokasi_id' AND DATE(a.tgl_periksa)='$tgl_periksa'
            ORDER BY a.regonline_id DESC
            LIMIT 1";
    $query = $this->db->query($sql, array($lokasi_id, $tgl_periksa));
    return $query->row_array();
  }

  public function count_reg_pasien_online($lokasi_id=null, $tgl_periksa=null) {
    $sql = "SELECT 
              COUNT(*) AS jml_data
            FROM 
              reg_pasien_online a 
            WHERE a.lokasi_id='$lokasi_id' AND DATE(a.tgl_periksa)='$tgl_periksa'";
    $query = $this->db->query($sql, array($lokasi_id, $tgl_periksa));
    $row = $query->row_array();
    return $row['jml_data'];
  }

  public function check_reg_pasien_online($pasien_nm=null, $no_telp='', $tgl_periksa='') {
    $sql = "SELECT 
              a.*
            FROM 
              reg_pasien_online a 
            WHERE a.pasien_nm=? AND a.no_telp=? AND DATE(a.tgl_periksa)=?";
    $query = $this->db->query($sql, array($pasien_nm, $no_telp, $tgl_periksa));
    if($query->num_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }

  public function no_rm_row($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    $row['status_cari'] = (@$row !='') ? '1' : '0';
    $row['wilayah_st'] = (@$row['wilayah_st'] !='') ? @$row['wilayah_st'] : '';
    $row['wilayah_id'] = (@$row['wilayah_id'] !='') ? @$row['wilayah_id'] : '';
    $row['tgl_lahir'] = (@$row['tgl_lahir'] !='') ? @$row['tgl_lahir'] : '';
    $row['sex_cd'] = (@$row['sex_cd'] !='') ? @$row['sex_cd'] : '';
    $row['no_telp'] = (@$row['no_telp'] !='') ? str_replace('62', '', @$row['no_telp']) : '';
    $row['jenispasien_id'] = (@$row['jenispasien_id'] !='') ? @$row['jenispasien_id'] : '';
    return $row;
  }

  public function save()
  {
    $data = html_escape($this->input->post());
    // get mst_lokasi
    $get_mst_lokasi = $this->get_mst_lokasi($data['lokasi_id']);
    // date
    $date_h1 = date('d-m-Y', strtotime(date('d-m-Y'). ' +1 days'));
    // get jadwal dokter
    $hari = strtolower(day(date('D', strtotime($data['tgl_periksa']))));
    $jadwal_dokter = $this->get_jadwal_dokter($data['lokasi_id'], $hari);
    // get reg_pasien_online
    $reg_pasien_online = $this->get_reg_pasien_online($data['lokasi_id'], toDate($data['tgl_periksa']));
    // count reg_pasien_online
    $count_reg_pasien_online = $this->count_reg_pasien_online($data['lokasi_id'], toDate($data['tgl_periksa']));

    //
    if ($reg_pasien_online['regonline_id'] =='') {
      $waktu_periksa = $jadwal_dokter['jam_buka'].':00';
      $antrian_no = '1';
    }else{
      $waktu_periksa = date('H:i:s',strtotime('+'.$jadwal_dokter['jeda_waktu'].' minutes',strtotime($reg_pasien_online['waktu_periksa'])));
      $antrian_no = $reg_pasien_online['antrian_no']+1;
    }

    $data['regonline_id'] = get_id('reg_pasien_online');
    $data['antrian_cd'] = $get_mst_lokasi['antrian_cd'];
    //
    // $get_lkt_antrian = $this->get_lkt_antrian($data['lokasi_id'], toDate($data['tgl_periksa']));
    // $data['antrian_no'] = ($get_lkt_antrian['antrian_no'] !='') ? $get_lkt_antrian['antrian_no']+1 : '1';
    $data['antrian_no'] = $antrian_no;
    //
    $data['tgl_lahir'] = toDate($data['tgl_lahir']);
    $data['tgl_periksa'] = toDate($data['tgl_periksa']).' '.$waktu_periksa;
    $data['provinsi'] = get_wilayah($data['wilayah_prop'], 'name');
    $data['kabupaten'] = get_wilayah($data['wilayah_kab'], 'name');
    $data['kecamatan'] = get_wilayah($data['wilayah_kec'], 'name');
    $data['kelurahan'] = get_wilayah($data['wilayah_kel'], 'name');
    $data['wilayah_id'] = get_wilayah($data['wilayah_kel'], 'id');
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = 'publik';
    // no telpon
    if (substr($data['no_telp'],0,1) == '0') {
      $data['no_telp'] = '62'.substr($data['no_telp'],1);
    }elseif (substr($data['no_telp'],0,2) == '62') {
      $data['no_telp'] = '62'.substr($data['no_telp'],2);
    }elseif (substr($data['no_telp'],0,3) == '+62') {
      $data['no_telp'] = '62'.substr($data['no_telp'],3);
    }else{
      $data['no_telp'] = '62'.$data['no_telp'];
    }
    // wa expired
    $wa_exp = $this->get_parameter('REGISTRASI', 'WA EXPIRED');
    $data['wa_exp'] = date('Y-m-d H:i:s',strtotime($wa_exp['parameter_val'],strtotime(date('Y-m-d H:i:s'))));
    // upload img
    $data['file_ktp'] = $this->upload_file_process($data, 'file_ktp');    
    $data['file_rujukan'] = $this->upload_file_process($data, 'file_rujukan');    
    // check reg_pasien_online
    $check = $this->check_reg_pasien_online($data['pasien_nm'], $data['no_telp'], $data['tgl_periksa']);
    // unset
    unset($data['wilayah_prop'], $data['wilayah_kab'], $data['wilayah_kec'], $data['wilayah_kel'], $data['rm'], $data['captcha_validate'], $data['captcha']);

    if ($data['file_ktp'] == 'error_img' || $data['file_rujukan'] == 'error_img') {
    	$result = array('id' => $data['regonline_id'], 'status' => 'error_img');
    }elseif ($check == true) {
      // unlink img
      unlink(assets_url('file_ktp') . $data['file_ktp']);
      unlink(assets_url('file_rujukan') . $data['file_rujukan']);
      
      $result = array('id' => $data['regonline_id'], 'status' => 'limit_reg');
    }elseif ($count_reg_pasien_online >= $jadwal_dokter['max_pasien']) {
      $result = array('status' => 'max_pasien');
    }else{
    	$this->db->insert('reg_pasien_online', $data);
      if ($reg_pasien_online['regonline_id'] !='') {
        $this->save_lkt_antrian($data, 'update');
      }else{
        $this->save_lkt_antrian($data, 'insert');
      }
    	update_id('reg_pasien_online', $data['regonline_id']);

    	$captcha = captcha(6);
    	$result_id = md5(md5($captcha)).md5(md5($data['regonline_id'])).md5(md5(md5(date('Y-m-d H:i:s'))));
    	$result = array('id' => $result_id, 'status' => 'success');
    }

    return $result;
  }

  public function save_lkt_antrian($data, $type) {
    $data_lkt_antrian['antrian_cd'] = $data['antrian_cd'];
    $data_lkt_antrian['antrian_no'] = $data['antrian_no'];
    $data_lkt_antrian['tgl_antrian'] = $data['tgl_periksa'];

    if ($type == 'insert') {
      $data_lkt_antrian['lokasi_id'] = $data['lokasi_id'];
      $data_lkt_antrian['created_at'] = date('Y-m-d H:i:s');
      $data_lkt_antrian['created_by'] = 'antrian_online';
      $result = $this->db->insert('lkt_antrian', $data_lkt_antrian);
    }elseif ($type == 'update') {
      $data_lkt_antrian['updated_at'] = date('Y-m-d H:i:s');
      $data_lkt_antrian['updated_by'] = 'antrian_online';
      $where = array(
                  'tgl_antrian' => $data['tgl_periksa'], 
                  'lokasi_id' => $data['lokasi_id'], 
                );
      $result = $this->db->where($where)->update('lkt_antrian', $data_lkt_antrian);
    }else{
      $result = '';
    }

    return $result;
  }

  public function upload_file_process($data=null,$file_name=null) {
    $result   = '';
    if(@$_FILES[$file_name]['tmp_name'] != '') {
        $path_dir       = assets_url($file_name);
        $tmp_name       = @$_FILES[$file_name]['tmp_name'];
        $fupload_name   = @$_FILES[$file_name]['name'];
        //
        $result = $this->_upload_img($path_dir, $tmp_name, $fupload_name, $file_name);      
    }        
    return $result;
  }

  public function _upload_img($path_dir=null, $tmp_name=null, $fupload_name=null, $file_name=null) {
    //
    $this->load->library('upload');
    $this->load->library('image_lib');
    //
    $name_img = create_title_img($path_dir, $tmp_name, $fupload_name);
    $src_file_name = $file_name;
    //
    $config['file_name'] = $name_img;
    $config['upload_path'] = $path_dir; //path folder
    $config['allowed_types'] = 'jpg|png|jpeg|gif'; //type yang dapat diakses bisa anda sesuaikan

    $this->upload->initialize($config);
    if(!empty($fupload_name)){
      if ($this->upload->do_upload($src_file_name)){
        $gbr = array('upload_data' => $this->upload->data()); 
        // cek resolusi gambar
        $nama_gambar = $path_dir.$gbr['upload_data']['file_name'];
        $data = getimagesize($nama_gambar);
        $width = $data[0];
        $height = $data[1];
        // pembagian
        $bagi_width = $width / 2;
        $bagi_height = $height / 2;
        // Compress Image
        $config['image_library'] ='gd2';
        $config['source_image'] = $gbr['upload_data']['full_path'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '20%';
        $config['width'] = round($bagi_width);
        $config['height'] = round($bagi_height);
        $config['new_image'] = $path_dir.$gbr['upload_data']['file_name'];
        $this->image_lib->initialize($config);
        // $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        //
        // $gambar_1 = $gbr['upload_data']['file_name'];
        $result = $name_img;
      }else{
        $result = 'error_img';
      }
      //
      return $result;          
    }   
  }
  
}