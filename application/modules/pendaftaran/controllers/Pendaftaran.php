<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends MY_Controller {

	var $menu;

	public function __construct(){
    parent::__construct();
		$this->load->model(array(
			'm_pendaftaran',
		));

		$this->menu = 'pendaftaran';

		$this->load->helper('cookie');
		$this->load->helper('string');

		// set cookie
		$cookie= array(
	       'name'   => '_t',
	       'value'  => md5(md5(md5(date('Y-m-d His').'_t'))),
	       'expire' => '3600',
		);
		$this->input->set_cookie($cookie);

		$cookie= array(
	       'name'   => '_token',
	       'value'  => md5(md5(md5(date('Y-m-d H:i:s').'_token'))),
	       'expire' => '3600',
		);
		$this->input->set_cookie($cookie);

		$cookie= array(
	       'name'   => '_validate',
	       'value'  => md5(md5(md5(date('Y-m-d Hi:s').'_validate'))),
	       'expire' => '3600',
		);
		$this->input->set_cookie($cookie);

		// set cookie
		$cookie= array(
	       'name'   => '_trash',
	       'value'  => md5(md5(md5(date('Y-m-d H').'_trash'))),
	       'expire' => '3600',
		);
		$this->input->set_cookie($cookie);
  }

	public function get()
	{
	   echo $this->input->cookie('_trash',true);
	}

	public function index()
	{
		$data['form_action'] = site_url().'/pendaftaran/save';
		$data['menu'] = $this->menu;
		$data['list_klinik_tujuan'] = $this->m_pendaftaran->list_klinik_tujuan();
		$data['list_jenis_pasien'] = $this->m_pendaftaran->list_jenis_pasien();
		$data['limit_tgl'] = $this->m_pendaftaran->limit_tgl();

		$data['trash'] = $this->input->cookie('_trash',true);

	  $this->render('index',$data);
	}

	public function resume($id='')
	{
		$data['identitas']   = $this->m_app->_get_identitas();
		$data['menu'] = $this->menu;
		$data['id'] = $id;
		if($id == '') {
			redirect(site_url());
		} else {
			$regonline_id = $this->m_pendaftaran->get_replace_id($id);
			$is_regonline_id = $this->m_pendaftaran->is_reg_pasien_online($regonline_id);
			if($is_regonline_id == false) {
				redirect(site_url());
			} else {
				$is_id = $is_regonline_id;
			}
		}

		$data['resume'] = $this->m_pendaftaran->get_resume($is_id);

		// qrcode
		$this->load->library('ciqrcode');
    $config['cacheable']    = true;
    $config['cachedir']     = 'images/qrcode/';
    $config['errorlog']     = 'images/qrcode/';
    $config['imagedir']     = 'images/qrcode/';
    $config['quality']      = true;
    $config['size']         = '1024';
    $config['black']        = array(224,255,255);
    $config['white']        = array(70,130,180);
    $this->ciqrcode->initialize($config);

    $params['data'] = $data['resume']['regonline_id'];
    $params['level'] = 'H';
    $params['size'] = 10;
    $params['savename'] = FCPATH.$config['imagedir'].'qrcode.png';
    $this->ciqrcode->generate($params);

	  $this->render('resume',$data);
	}

	public function download_bukti_pdf($id=null) {
		$identitas = $this->m_app->_get_identitas();
		$profile = $this->m_app->_get_config();

		
		//generate pdf
    $this->load->library('pdf');
    $pdf = new Pdf('p','mm',array(210,297));
    $pdf->AliasNbPages();
    $pdf->SetTitle('Cetak Kwitansi '.$id);
    $pdf->AddPage();
    
    $pdf->Image(FCPATH.'assets/images/icon/'.$identitas['logo_rumah_sakit'],10,10,15,15);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(18,6,'',0,0,'L');
		$pdf->Cell(0,4,$profile['title_logo_login'],0,1,'L');
    $pdf->SetFont('Arial','B',13);
		$pdf->Cell(18,6,'',0,0,'L');
		$pdf->Cell(0,6,$profile['sub_title_logo_login'],0,1,'L');
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(18,6,'',0,0,'L');
    $pdf->Cell(0,4,@$identitas['jalan'].', '.ucfirst(strtolower(clear_kab_kota(@$identitas['kabupaten']))).', '.ucfirst(strtolower(@$identitas['propinsi'])),0,1,'L');
		$pdf->Line(10, 28, 200, 28);
		
		$pdf->Cell(0,8,'',0,1,'L');
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(100,5,'INVOICE',0,0,'L');
		$pdf->SetFont('Arial','B',9);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,5,'BIAYA PERAWATAN RAWAT INAP',0,1,'L');
		$pdf->Cell(0,5,'',0,1,'C');

		//
    
    $pdf->Output('I','Kwitansi_'.$id.'_'.date('Ymdhis').'.pdf');
	}

	public function error($status='')
	{
		$data['menu'] = $this->menu;
		$data['status'] = $status;
		$data['decrypt_status'] = decrypt_status($status);

	  $this->render('error',$data);
	}

	public function save()
	{
		$save = $this->m_pendaftaran->save();
		if ($save['status'] == 'error_img') {
			redirect(site_url().'/pendaftaran/error/'.create_status($save['status'], 'full'));
		}elseif ($save['status'] == 'limit_reg') {
			redirect(site_url().'/pendaftaran/error/'.create_status($save['status'], 'full'));
		}elseif ($save['status'] == 'max_pasien') {
			redirect(site_url().'/pendaftaran/error/'.create_status($save['status'], 'full'));
		}else{
			redirect(site_url().'/pendaftaran/resume/'.$save['id']);
		}
	}

	public function ajax($id=null) {
		if($id == 'get_wilayah_child') {
			$wilayah_parent = $this->input->get('wilayah_parent');
			$wilayah_prop = $this->input->get('wilayah_prop');
			$wilayah_kab = $this->input->get('wilayah_kab');
			$wilayah_kec = $this->input->get('wilayah_kec');
			$wilayah_kel = $this->input->get('wilayah_kel');
			//
			$wilayah_params = $this->m_pendaftaran->get_params($wilayah_parent);
			//
			$wilayah_selected = '';
			if($wilayah_params['level'] == '2' && $wilayah_kab != '') { // kab
				$wilayah_selected = $wilayah_kab;
			} else if($wilayah_params['level'] == '3' && $wilayah_kec != '') { // kec
				$wilayah_selected = $wilayah_kec;
			} else if($wilayah_params['level'] == '4' && $wilayah_kel != '') { // kel
				$wilayah_selected = $wilayah_kel;
			}
			//
			$list_wilayah = $this->m_pendaftaran->list_wilayah_by_parent($wilayah_parent);
			//
			$html = '';
			$html .= '<select name="'.$wilayah_params['input_nm'].'" id="'.$wilayah_params['input_nm'].'" class="form-control select2" required="">';
			$html .= '<option value="">-- Pilih --</option>';
			foreach($list_wilayah as $w) {
				if($wilayah_selected == $w['wilayah_id']) {
					$html .= '<option value="'.$w['wilayah_id'].'#'.$w['wilayah_nm'].'" selected>'.$w['wilayah_nm'].'</option>';
				} else {
					$html .= '<option value="'.$w['wilayah_id'].'#'.$w['wilayah_nm'].'">'.$w['wilayah_nm'].'</option>';
				}				
			}
			$html .= '</select>';
			$html .= '<script>
					  $(function() {
					  ';
					  // autoload
					  if($wilayah_selected != '') {
			$html .= 	'_get_wilayah("'.@$wilayah_selected.'","'.@$wilayah_prop.'", "'.@$wilayah_kab.'", "'.@$wilayah_kec.'", "'.@$wilayah_kel.'"); ';					  	
					  } 
					  // 
			$html .= '	$("#'.$wilayah_params['input_nm'].'").bind("change",function(e) {
					  		e.preventDefault();
					  		var i = $(this).val().split("#");
	    					_get_wilayah(i[0]);
					  	});
					  	//
					  	function _get_wilayah(i, prop="", kab="", kec="", kel="") {
					  		var ext_var = "wilayah_prop="+prop;
				                ext_var+= "&wilayah_kab="+kab;
				                ext_var+= "&wilayah_kec="+kec;
				                ext_var+= "&wilayah_kel="+kel;

					  		$.get("'.site_url('pendaftaran/ajax/get_wilayah_child').'?wilayah_parent="+i+"&"+ext_var,null,function(data) {
					  			$("#box_'.$wilayah_params['input_nm_next'].'").html(data.html);
					  		},"json");
					  	}
					  });
					  </script>
					 ';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}elseif ($id == 'cek_captcha') {
			$data = $this->input->post();
			if ($data['captcha'] == $data['captcha_validate']) {
				echo 'true';
			}else{
				echo 'false';
			}
		}elseif($id == 'get_provinsi_by_st') {
			$wilayah_st = $this->input->get('wilayah_st');
			if ($wilayah_st == 'D') {
				$get_identitas = $this->m_pendaftaran->get_identitas();
				$list_wilayah_prop = $this->m_pendaftaran->list_wilayah_by_id($get_identitas['wilayah_prop']);
			}elseif ($wilayah_st == 'L'){
				$list_wilayah_prop = $this->m_pendaftaran->list_wilayah_by_parent('');
			}
			//
			$html = '';
			$html .= '<select name="wilayah_prop" id="wilayah_prop" class="form-control select2 w-100" required="">';
			$html .= '<option value="">-- Pilih --</option>';
			foreach($list_wilayah_prop as $w) {
				$html .= '<option value="'.$w['wilayah_id'].'#'.$w['wilayah_nm'].'">'.$w['wilayah_nm'].'</option>';			
			}
			$html .= '</select>';
			$html .= '<script>
					  $(function() {
					  '; 
					  // 
			$html .= ' $("#wilayah_prop").bind("change",function(e) {
				      e.preventDefault();
				      var i = $(this).val().split("#");
				      _get_wilayah(i[0]);
				    })
				    //
				    function _get_wilayah(i, prop="", kab="", kec="", kel="") {
				      var ext_var = "wilayah_prop="+prop;
				          ext_var+= "&wilayah_kab="+kab;
				          ext_var+= "&wilayah_kec="+kec;
				          ext_var+= "&wilayah_kel="+kel;
				      $.get("'.site_url("pendaftaran/ajax/get_wilayah_child").'?wilayah_parent="+i+"&"+ext_var,null,function(data) {
				          $("#box_wilayah_kab").html(data.html);
				      },"json");
				    }
					  });
					  </script>
					 ';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'empty_kabupaten') {
			$html = '';
			$html .= '<select name="wilayah_kab" id="wilayah_kab" class="chosen-select select2 w-100" required="">';
			$html .= '<option value="">- Kab/Kota -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'empty_kecamatan') {
			$html = '';
			$html .= '<select name="wilayah_kec" id="wilayah_kec" class="chosen-select select2 w-100" required="">';
			$html .= '<option value="">- Kecamatan -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'empty_kelurahan') {
			$html = '';
			$html .= '<select name="wilayah_kel" id="wilayah_kel" class="chosen-select select2 w-100" required="">';
			$html .= '<option value="">- Desa/Kelurahan -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'get_wilayah_id_name') {
			$wilayah_parent = $this->input->get('wilayah_parent');
			$wilayah_prop = $this->input->get('wilayah_prop');
			$wilayah_kab = $this->input->get('wilayah_kab');
			$wilayah_kec = $this->input->get('wilayah_kec');
			$wilayah_kel = $this->input->get('wilayah_kel');
			//
			$wilayah_params = $this->m_pendaftaran->get_params($wilayah_parent);
			//
			$wilayah_selected = '';
			if($wilayah_params['level'] == '2' && $wilayah_kab != '') { // kab
				$wilayah_selected = $wilayah_kab;
			} else if($wilayah_params['level'] == '3' && $wilayah_kec != '') { // kec
				$wilayah_selected = $wilayah_kec;
			} else if($wilayah_params['level'] == '4' && $wilayah_kel != '') { // kel
				$wilayah_selected = $wilayah_kel;
			}
			//
			$list_wilayah = $this->m_pendaftaran->list_wilayah_by_parent($wilayah_parent);
			//
			$html = '';
			if ($wilayah_parent == '' && $wilayah_prop == '' && $wilayah_kab == '' && $wilayah_kec == '' && $wilayah_kel == '') {
				$html .= '<select class="chosen-select select2 w-100" name="wilayah_kab" id="wilayah_kab" required="">
                    <option value="">- Kab/Kota -</option>
                  </select>';
			}else{
				$html .= '<select name="'.$wilayah_params['input_nm'].'" id="'.$wilayah_params['input_nm'].'" class="chosen-select select2 w-100" required="">';
				$html .= '<option value="">- '.$wilayah_params['level_nm'].' -</option>';
				foreach($list_wilayah as $w) {
					if($wilayah_selected == $w['wilayah_id']) {
						$html .= '<option value="'.$w['wilayah_id'].'#'.$w['wilayah_nm'].'" selected>'.$w['wilayah_id'].' - '.$w['wilayah_nm'].'</option>';
					} else {
						$html .= '<option value="'.$w['wilayah_id'].'#'.$w['wilayah_nm'].'">'.$w['wilayah_id'].' - '.$w['wilayah_nm'].'</option>';
					}				
				}
				$html .= '</select>';
				$html .= '<script>
						  $(function() {
						  ';
						  // autoload
						  if($wilayah_selected != '') {
				$html .= 	'_get_wilayah("'.@$wilayah_selected.'","'.@$wilayah_prop.'", "'.@$wilayah_kab.'", "'.@$wilayah_kec.'", "'.@$wilayah_kel.'"); ';					  	
						  } 
						  // 
				$html .= '	$("#'.$wilayah_params['input_nm'].'").bind("change",function(e) {
						  		e.preventDefault();
						  		var i = $(this).val().split("#");
		    					_get_wilayah(i[0]);
						  	});
						  	//
						  	function _get_wilayah(i, prop="", kab="", kec="", kel="") {
						  		var ext_var = "wilayah_prop="+prop;
					                ext_var+= "&wilayah_kab="+kab;
					                ext_var+= "&wilayah_kec="+kec;
					                ext_var+= "&wilayah_kel="+kel;

						  		$.get("'.site_url('pendaftaran/ajax/get_wilayah_id_name').'?wilayah_parent="+i+"&"+ext_var,null,function(data) {
						  			$("#box_'.$wilayah_params['input_nm_next'].'").html(data.html);
						  		},"json");
						  	}
						  });
						  </script>
						 ';
			}
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'no_rm_fill'){
			$data = $this->input->post();
			$res = $this->m_pendaftaran->no_rm_row($data['pasien_id']);
			echo json_encode($res);
		}
	}

	public function token()
	{
		$data = $this->input->post();
		$token = md5(md5(md5(date('Y-m-d H').'_trash')));
		if (@$data['c'] == $token) {
			$this->load->library('xmlrpc');
			$this->load->library('xmlrpcs');
			$url = "https://ayokitakerja.kemnaker.go.id/tools/check_nik/".$data['nik'];

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_FAILONERROR, true);
			$data = curl_exec($ch);

			if (curl_errno($ch))
			{
				$res = array('STATUS' => -1);
				echo json_encode($res);
			}else{
				$data = json_decode($data, TRUE);
				curl_close($ch);
				echo json_encode($data);
			}
		}else{
			$res = array('STATUS' => -2);
			echo json_encode($res);
		}
	}
  
}
