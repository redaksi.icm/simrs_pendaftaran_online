<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_app extends CI_Model {

  function __construct(){
    parent::__construct();
  }

  function _get_config() {
    $sql = "SELECT * FROM app_config";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row;
  }

  function _get_identitas() {
    $sql = "SELECT * FROM mst_identitas";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row;
  }

}