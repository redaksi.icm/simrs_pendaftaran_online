<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>PENDAFTARAN ONLINE - <?=@$identitas['rumah_sakit']?></title>
  <link href="<?=base_url()?>plugins/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url()?>dist/css/fonts.css" rel="stylesheet">
  <link href="<?=base_url()?>dist/css/style.css" rel="stylesheet">
  <link href="<?=base_url()?>plugins/select2/css/select2.css" rel="stylesheet">
  <link href="<?=base_url()?>plugins/select2-bootstrap4-theme/select2-bootstrap.css" rel="stylesheet">
  <link href="<?=base_url()?>plugins/daterangepicker/daterangepicker.css" rel="stylesheet">
  <link href="<?=base_url()?>plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
  <link href="<?=base_url()?>plugins/sweetalert2/sweetalert2.css" rel="stylesheet">
  <link href="<?=base_url()?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.css" rel="stylesheet">
  <link href="<?=base_url()?>plugins/icheck/css/square.css" rel="stylesheet">
  <link href="<?=base_url()?>plugins/icheck/css/green.css" rel="stylesheet">

  <script src="<?=base_url()?>plugins/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>plugins/jquery-easing/jquery.easing.min.js"></script>
  <script src="<?=base_url()?>plugins/select2/js/select2.full.js"></script>
  <script src="<?=base_url()?>plugins/daterangepicker/moment.min.js"></script>
  <script src="<?=base_url()?>plugins/jquery-validation/jquery.validate.min.js"></script>
  <script src="<?=base_url()?>plugins/jquery-validation/additional-methods.js"></script>
  <script src="<?=base_url()?>plugins/jquery-validation/localization/messages_id.js"></script>
  <script src="<?=base_url()?>plugins/daterangepicker/daterangepicker.js"></script>
  <script src="<?=base_url()?>plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
  <script src="<?=base_url()?>plugins/sweetalert2/sweetalert2.js"></script>
  <script src="<?=base_url()?>plugins/icheck/js/icheck.js"></script>
  <script src="<?=base_url()?>dist/js/rpn_helper.js"></script>
</head>