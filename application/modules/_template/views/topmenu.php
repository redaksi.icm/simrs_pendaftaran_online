<body id="page-top">
<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-secondary fixed-top" id="mainNav">
  <div class="container">
    
      
    <img src="<?=base_url()?>images/simrs-logo-rs.jpg" class="rounded-circle img-topbar">
    <a class="navbar-brand js-scroll-trigger" href="#page-top"><?=@$identitas['rumah_sakit']?></a>
    <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-bars"></i>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item mx-0 mx-lg-1">
          <a class="nav-link py-2 px-0 px-lg-2 rounded js-scroll-trigger <?php if('home' == $menu){echo 'active';}?>" href="<?=site_url()?>">
            <i class="fas fa-home"></i>
            Home
          </a>
        </li>
        <li class="nav-item mx-0 mx-lg-1">
          <a class="nav-link py-2 px-0 px-lg-2 rounded js-scroll-trigger <?php if('jadwal_dokter' == $menu){echo 'active';}?>" href="<?=site_url('jadwal_dokter')?>">
            <i class="fas fa-calendar-alt"></i>
            Jadwal Dokter
          </a>
        </li>
        <li class="nav-item mx-0 mx-lg-1">
          <a class="nav-link py-2 px-0 px-lg-2 rounded js-scroll-trigger <?php if('pendaftaran' == $menu){echo 'active';}?>" href="<?=site_url('pendaftaran')?>">
            <i class="fas fa-file-contract"></i>
            Pendaftaran
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>