  <!-- Copyright Section -->
  <section class="copyright py-4 text-center text-white">
    <div class="container">
      <small><?=@$config['copyright']?></small>
    </div>
  </section>

  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
  <div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
      <i class="fa fa-chevron-up"></i>
    </a>
  </div>
  <script src="<?=base_url()?>dist/js/freelancer.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $.fn.select2.defaults.set( "theme", "bootstrap" );
      $('.select2').select2();
      $('.select2-hidden').select2({
        minimumResultsForSearch: Infinity
      });

      $('input[type=radio]').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%'
      });
    })
  </script>
</body>
</html>