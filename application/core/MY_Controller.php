<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller{

  function __construct(){
    parent::__construct();
  }

  function render($content, $data = NULL){
    $data['config']   = $this->m_app->_get_config();
    $data['identitas']   = $this->m_app->_get_identitas();
    // Templating
    $data['header'] = $this->load->view('_template/header', $data, TRUE);
    $data['topmenu'] = $this->load->view('_template/topmenu', $data, TRUE);
    $data['content'] = $this->load->view($content, $data, TRUE);
    $data['footer'] = $this->load->view('_template/footer', $data, TRUE);

    $this->load->view('_template/index', $data);
  }

}

class MY_Error extends MX_Controller{

  function render($content, $data = NULL){
    $data['header'] = $this->load->view('_template/error/header', $data, TRUE);
    $data['footer'] = $this->load->view('_template/error/footer', $data, TRUE);
    $data['content'] = $this->load->view($content, $data, TRUE);

    $this->load->view('_template/error/index', $data);
  }

}
