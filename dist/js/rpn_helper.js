function to_date(date='',sp='-',tp='',sp2=' ') {
  result = '';
  if(date != '' && date != null && date != 'null') {
    if(tp == 'date') {
      arr_date = date.split(' ');
      date = arr_date[0];
    } else if(tp == 'full_date') {
      arr_date = date.split(' ');
      date = arr_date[0];
      time = arr_date[1];
    }
    arr = date.split('-');
    if(sp != '') {
      result = arr[2]+sp+arr[1]+sp+arr[0];
    } else {
      result = arr[2]+'-'+arr[1]+'-'+arr[0];
    }
    if(tp == 'full_date') {
      if (sp2 !='') {
        result = result+sp2+time;
      }else{
        result = result+' '+$time;
      }
    }
  } else {
    result = '';
  }   
  return result;
}

function get_age(dateString) {
  var now = new Date();
  var today = new Date(now.getYear(),now.getMonth(),now.getDate());

  var yearNow = now.getYear();
  var monthNow = now.getMonth();
  var dateNow = now.getDate();

  var dob = new Date(dateString.substring(0,4),
                     dateString.substring(5,7)-1,                   
                     dateString.substring(8,10)                  
                     );

  var yearDob = dob.getYear();
  var monthDob = dob.getMonth();
  var dateDob = dob.getDate();
  var age = {};
  var ageString = "";
  var yearString = "";
  var monthString = "";
  var dayString = "";


  yearAge = yearNow - yearDob;

  if (monthNow >= monthDob)
    var monthAge = monthNow - monthDob;
  else {
    yearAge--;
    var monthAge = 12 + monthNow -monthDob;
  }

  if (dateNow >= dateDob)
    var dateAge = dateNow - dateDob;
  else {
    monthAge--;
    var dateAge = 31 + dateNow - dateDob;

    if (monthAge < 0) {
      monthAge = 11;
      yearAge--;
    }
  }

  age = {
    years: yearAge,
    months: monthAge,
    days: dateAge
  };

  return age;
}

function num_sys(x) {
  if(x === null){
    return 0;
  }else{
    x = x.replace(/\./g, "");
    x = x.replace(',', '.');
    return x;
  }
}

function num_id(bilangan) {
  if (bilangan === null) {
    return 0;
  }else{
    var negativ = false;
  
    if (bilangan < 0) {
      bilangan = bilangan * -1;
      negativ = true;
    };
  
    var number_string = bilangan.toString(),
      split = number_string.split('.'),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);
  
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
  
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  
    // Cetak hasil
    if (negativ == true) {
      rupiah = '-' + rupiah;
    }
    return rupiah;
  }
}